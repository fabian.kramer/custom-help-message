Check out my Profile at de.bukkit.org: [Link](https://dev.bukkit.org/members/funky33/projects)   
Download stable Jar-file at: [Link](https://dev.bukkit.org/projects/custom-help-message)

English:

Whith this Plugin you can change the Message of /help for the whole Server.

This means you make a different single Message not a design for normal /help.

 

Commands:

/help  --  Overrides the Bukkit Help as long as you use this Plugin

 

Permissions:

null

 

If you think I should upload more different versions of the Plugin please write a comment.

 

German:

Mit diesem Plugin ist ein Einfaches die Nachricht des /help Befehls zu ändern.

Das bedeutet, dass eine neue Nachricht erstellt wird und nicht ein Design für den normalen Befehl.

 

Befehle:

/help  --  Überschreibt den normalen Bukkit /help Befehl solange das Plugin installiert ist.

 

Permissions:

keine